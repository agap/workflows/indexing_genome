# Snakemake workflow: Indexing Genome

[![Snakemake](https://img.shields.io/badge/snakemake-≥6.3.0-brightgreen.svg)](https://snakemake.github.io)    
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

**Table of Contents**
 
  - [Objective](#objective)
  - [Dependencies](#dependencies)
  - [Overview of workflow](#overview-of-workflow)
  - [Procedure](#procedure)
  - [Output directory](#output-directory)
 
## Objective

Indexing Genome assembly (fasta) and Gene annotation (gff3 & fasta)

## Dependencies

* diamond : DIAMOND is a sequence aligner for protein and translated DNA searches, designed for high performance analysis of big sequence data. (https://github.com/bbuchfink/diamond) 
* hisat2 : Graph-based alignment of next generation sequencing reads to a population of genomes
(https://daehwankimlab.github.io/hisat2/manual/)
* ncbi-blast : Basic Local Alignment Search Tool  (https://blast.ncbi.nlm.nih.gov/Blast.cgi)
* samtools : Utilities for the Sequence Alignment/Map (SAM) format (https://www.htslib.org/doc/samtools.html)
* picard-tools : A set of command line tools (in Java) for manipulating high-throughput sequencing (HTS) data and formats such as SAM/BAM/CRAM and VCF (https://broadinstitute.github.io/picard/)
* tabix : Generic indexer for TAB-delimited genome position files (http://www.htslib.org/doc/tabix.html)
* bowtie2 : A fast and sensitive gapped read aligner (https://github.com/BenLangmead/bowtie2)
* meme suite
* agat : Suite of tools to handle gene annotations in any GTF/GFF format. (https://github.com/NBISweden/AGAT)

## Prerequisites

- Globally installed SLURM 18.08.7.1+
- Globally installed singularity 3.4.1+
- Installed Snakemake 6.0.0+

## Overview of workflow

<p align="center">
<img src="images/dag.png" width="50%">
</p>


## Procedure 

- Clone the code repository from github to your computer by running the
  following shell command in your terminal:
```bash
git clone https://gitlab.cirad.fr/agap/cluster/snakemake/indexing_genome.git
```


- Change directories into the cloned repository:

```bash
cd indexing_genome
```

- Edit the configuration file config.yaml

```bash
# input_genome = For each organism defined by the first keys (i.e, Elaeis_guineensis), you need to provide several files or values
# All outputs should be prefixed with key name
# - gene = Translated nucleotide sequence as fasta
# - cds = Translated nucleotide sequence as fasta
# - prot = Translated nucleotide sequence as fasta
# - assembly = Assembly as fasta
# - gff3 = Annotation file in GFF format, for each mRNA, we used the tag value Name which should be same as fasta file   
# - name = Species name 
# - jbrowse_instance = Name of the (future) JBrowse directory

input_genome:
  "Elaeis_guineensis":
    "gene": "/storage/replicated/cirad/web/HUBs/palm/elaeis_guineensis/EG_LM2T_REF_Cirad_v1_gene.fna"
    "cds": "/storage/replicated/cirad/web/HUBs/palm/elaeis_guineensis/EG_LM2T_REF_Cirad_v1_cds.fna"
    "prot": "/storage/replicated/cirad/web/HUBs/palm/elaeis_guineensis/EG_LM2T_REF_Cirad_v1_protein.faa"
    "assembly": "/storage/replicated/cirad/web/HUBs/palm/elaeis_guineensis/EG_LM2T_REF_Cirad_v1.fna" 
    "gff3": "/storage/replicated/cirad/web/HUBs/palm/elaeis_guineensis/EG_LM2T_REF_Cirad_v1_annoted.gff3"
    "name": "E. guineensis" 
    "jbrowse_instance": "elaeis_guineensis"
```

## For Meso@LR

- Load module
```bash
module load snakemake/7.15.1-conda
module load singularity/3.5
```

- Print shell commands to validate your modification (mode dry-run)

```bash
sbatch snakemake dry
```

- Run workflow on Meso@LR

```bash
sbatch snakemake.sh run
```
     
- Unlock directory

```bash
sbatch snakemake.sh unlock
```
      
- Generate DAG file

```bash
sbatch snakemake.sh dag
```
             
## Output directory : 

- results/Elaeis_guineensis

Copy this directory to /storage/replicated/cirad/web/HUBs/