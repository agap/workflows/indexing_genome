import snakemake
import random
import os
import shutil
import sys
configfile: "config.yaml"
 

# Globals ---------------------------------------------------------------------
input_genome=config["input_genome"]


list_genome=input_genome.keys()
 

wildcard_constraints:
    database = "|".join(input_genome.keys())


  
rule all:
    input: 
        expand("results/{database}/{database}.assembly.fasta.fai", database=input_genome.keys()), 
        expand("results/{database}/{database}.assembly.fasta.index", database=input_genome.keys()),
        expand("results/{database}/{database}.assembly.dict", database=input_genome.keys()),
        expand("results/{database}/{database}.assembly.fasta.ndb", database=input_genome.keys()), 
        expand("results/{database}/{database}.assembly.fasta.1.ht2", database=input_genome.keys()), 
        expand("results/{database}/{database}.assembly.fasta.1.bt2", database=input_genome.keys()), 
        expand("results/{database}/{database}.cds.fna.ndb", database=input_genome.keys()),
        expand("results/{database}/{database}.cds.fna.index", database=input_genome.keys()), 
        expand("results/{database}/{database}.gene.fna.ndb", database=input_genome.keys()),
        expand("results/{database}/{database}.gene.fna.index", database=input_genome.keys()),  
        expand("results/{database}/{database}.protein.faa.pdb", database=input_genome.keys()),
        expand("results/{database}/{database}.protein.faa.index", database=input_genome.keys()),
        expand("results/{database}/{database}.protein.faa.dmnd", database=input_genome.keys()), 
        expand("results/{database}/{database}.gff3.gz", database=input_genome.keys())

rule update_assembly:
    input:
        lambda wildcards: input_genome[wildcards.database]["assembly"]
    output:
        fasta = "results/{database}/{database}.assembly.fasta",
        index = "results/{database}/{database}.assembly.fasta.index"  
    params:
        jbrowse_instance = lambda wildcards: input_genome[wildcards.database]["jbrowse_instance"]
    singularity:
        config["containers"]["perllib"] 
    shell:"""
        script/update_assembly.pl {input} {output.fasta} {params.jbrowse_instance}
    """         

rule check_obo:
    input:
        lambda wildcards: input_genome[wildcards.database]["gff3"]
    output:
        temp("results/{database}/{database}_temp.gff3")
    singularity:
        config["containers"]["perllib"] 
    shell:"""
        script/check_obo.pl -in {input} -out {output}
    """

     
rule samtools_faidx:
    input:
        rules.update_assembly.output.fasta
    output: 
        "results/{database}/{database}.assembly.fasta.fai"  
    singularity:
        config["containers"]["samtools"] 
    shell:""" 
        samtools faidx {input} 
    """
   
rule makeblastdb_assembly:
    input:
        rules.update_assembly.output.fasta
    output: 
        "results/{database}/{database}.assembly.fasta.ndb"  
    singularity:
        config["containers"]["blast"] 
    shell:""" 
        makeblastdb -in {input} -dbtype nucl  -title 'Assembly'
    """

rule hisat2_build:
    input:
        rules.update_assembly.output.fasta
    output: 
        "results/{database}/{database}.assembly.fasta.1.ht2"  
    singularity:
        config["containers"]["hisat2"]
    threads:
        config["threads"]
    shell:""" 
        hisat2-build -p {threads} {input} {input}
    """

rule bowtie2_build:
    input:
        rules.update_assembly.output.fasta
    output: 
        "results/{database}/{database}.assembly.fasta.1.bt2"  
    singularity:
        config["containers"]["bowtie2"]
    threads:
        config["threads"]
    shell:""" 
        bowtie2-build --threads {threads} {input} {input}
    """
  
rule picard_create_sequence_dictionary:
    input:
        rules.update_assembly.output.fasta
    output: 
        "results/{database}/{database}.assembly.dict"  
    singularity:
        config["containers"]["picard"] 
    shell:""" 
        picard CreateSequenceDictionary R={input} O={output}
    """
  

# GFF3 need to be properly sorted for indexing with tabix
rule gff3sort:
    input:
        rules.check_obo.output
    output:
        "results/{database}/{database}.gff3" 
    singularity:
        config["containers"]["perllib"]
    shell:"""
        script/gff3sort.pl --precise {input} > {output}
    """ 

# Indexing GFF before integration on JBrowse
rule tabix:
    input:
        rules.gff3sort.output
    output:
        gz = "results/{database}/{database}.gff3.gz",
        tbi = "results/{database}/{database}.gff3.gz.tbi" 
    singularity:
        config["containers"]["tabix"]
    
    shell:"""
       bgzip -c {input} > {output.gz};
       tabix -p gff {output.gz} 
    """ 

rule get_fasta:
    input:
        lambda wildcards: input_genome[wildcards.database]["gene"]
    output:
        "results/{database}/{database}.gene.fna"
    shell:"""
        cp {input} {output}
    """

use rule get_fasta as get_fasta_prot with:
    input:
        lambda wildcards: input_genome[wildcards.database]["prot"]
    output:
        "results/{database}/{database}.protein.faa"

use rule get_fasta as get_fasta_cds with:
    input:
        lambda wildcards: input_genome[wildcards.database]["cds"]
    output:
        "results/{database}/{database}.cds.fna"

rule makeblastdb_gene:
    input:
        gene = rules.get_fasta.output,
        cds = rules.get_fasta_cds.output,
        prot = rules.get_fasta_prot.output
    output: 
        gene_index = "results/{database}/{database}.gene.fna.ndb",  
        cds_index = "results/{database}/{database}.cds.fna.ndb", 
        prot_index = "results/{database}/{database}.protein.faa.pdb"  
    singularity:
        config["containers"]["blast"] 
    shell:""" 
        makeblastdb -in {input.gene} -dbtype nucl -parse_seqids -title 'Predicted gene sequences, including exons, introns and upstream/downstream untranslated region';
        makeblastdb -in {input.cds} -dbtype nucl -parse_seqids -title 'Predicted transcripts (coding sequence only)';
        makeblastdb -in {input.prot} -dbtype prot -parse_seqids -title 'Translate coding region';
    """

rule fasta_make_index:
    input:
        gene = rules.get_fasta.output,
        cds = rules.get_fasta_cds.output,
        prot = rules.get_fasta_prot.output
    output: 
        gene_index = "results/{database}/{database}.gene.fna.index",
        cds_index = "results/{database}/{database}.cds.fna.index", 
        prot_index = "results/{database}/{database}.protein.faa.index"
    singularity:
        config["containers"]["meme"] 
    shell:""" 
        fasta-make-index {input.gene} -f  ;
        fasta-make-index {input.cds} -f ;
        fasta-make-index {input.prot} -f 
    """

rule diamond_prepdb:
    input:
        prot = rules.get_fasta_prot.output
    output:  
        prot_index = "results/{database}/{database}.protein.faa.dmnd"
    singularity:
        config["containers"]["diamond"] 
    shell:""" 
        diamond makedb --in {input.prot} -d {input.prot}
    """

   