#!/usr/bin/env perl
use Bio::SeqIO;
use Bio::DB::Fasta;
my $fasta_in = shift;
my $fasta_out = shift;
my $jbrowse_instance = shift;
my $in = new Bio::SeqIO(
    -file => $fasta_in,
    -format => 'fasta'
); 
my $out = new Bio::SeqIO(
    -file => ">$fasta_out",
    -format => "fasta"
);
while (my $seqobj = $in->next_seq){
    my $display_id = $seqobj->display_id();
    $seqobj->desc("");
    $seqobj->desc($jbrowse_instance);
    $out->write_seq($seqobj);
}
$out->close;
$in->close;
my $db_fasta = Bio::DB::Fasta->new($fasta_out)