#!/usr/bin/env perl

use Bio::Tools::GFF;
use FindBin;
use Getopt::Long;
use Cwd;
use Pod::Usage; 
my $gff_in = "";
my $gff_out = "";
my $log_file = "";
my $help = "";
my $usage = $FindBin::Bin ."/". $FindBin::Script.q/ --help

Parameters
    -in   GFF3 in [required]
    -out  GFF3 out [required] 
    -log  Log file
    -help
/;
GetOptions(
    'in=s'      => \$gff_in,
    'out=s'     => \$gff_out,
    'log=s'    => \$log_file, 
    'help|h|?' => \$help
)  or pod2usage(1);
if ($help) { pod2usage(1); } 

my $gffio = Bio::Tools::GFF->new(
  -file => $gff_in,
  -gff_version => 3
);

my $gffout = Bio::Tools::GFF->new(
  -file => ">$gff_out",
  -gff_version => 3
);
my $obo = "go.obo";
unless (-e $obo) {
  system("wget -q -O $obo https://purl.obolibrary.org/obo/go.obo");
}
open(IN,$obo);
my %go;
my $term;
while(<IN>){
  chomp;
  if ($_ =~ /^id:\s(GO:\d+)/){
    $term = $1;
  }
  if ($_ =~ /^name:\s(.*)/){
    $go{$term} = $1;
  }
}
close IN;
my %ontology;
while(my $feature = $gffio->next_feature()) {
  if ($feature->primary_tag() eq "mRNA"){
    if ($feature->has_tag("Ontology_term")) {
      my @ontology = $feature->get_tag_values("Ontology_term");
      $feature->remove_tag("Ontology_term");
      foreach my $ontology (@ontology) {
        if ($go{$ontology}){
          $feature->add_tag_value("Ontology_term",$ontology);
        } 
        else {
          $delete{$ontology}++;
        }
      }
    } 
    $gffout->write_feature($feature);
  }
  else {
    next if ($feature->primary_tag() eq "polypeptide");
    $gffout->write_feature($feature);
  }
}
$gffio->close();
system("rm $obo");
open(LOG,">$log_file");
foreach my $ontology (keys %delete) {
  print LOG join("\t",$ontology,$delete{$ontology},$go{$ontology} ) ,"\n";
}
close LOG;